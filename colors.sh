#!/bin/bash
printf "\033[0;31mA vida é tão curta pra deixar \n"
sleep 1
printf "\033[0;32mde amar e de perdorar. \n"
sleep 1
printf "\033[0;33mAproveite cada minuto, \n"
sleep 1
printf "\033[0;34mdeixe o orgulho de lado, \n"
sleep 1
printf "\033[0;35mviva intensamente, \n"
sleep 1
printf "\033[0;36msorria sem limites, \n"
sleep 1
printf "\033[0;31mabraçe quem você ama, \n"
sleep 1
printf "\033[0;32mum aperto de mão, \n"
sleep 1
printf "\033[0;33muma palavra amiga, \n"
sleep 1
printf "\033[0;34mum olhar de amor, \n"
sleep 1
printf "\033[0;35muma história linda. \n"
sleep 1
printf "\033[0;37m - de TataInPoesias\n"
